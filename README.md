# Docker Build Scripts

Useful/public Docker builds for ShowerFM

* **rpi-kivy** – A RaspberryPi build of Raspbian/Jessie, with python, kivy, and gstreamer already installed.
* **rpi-node-coffee** – A RaspberryPi build of Raspbian/Jessie, with nodejs, coffeescript already installed.
