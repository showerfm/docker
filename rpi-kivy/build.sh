#!/bin/bash

name=$(basename $(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd))

info() {
  printf "%s\n" "$@"
}

fatal() {
  printf "**********\n"
  printf "%s\n" "$@"
  printf "**********\n"
  exit 1
}

echo "Building $name..."
docker build -q -t madleech/$name .

if [[ $? -gt 0 ]]; then
	fatal "Build of $name failed!"
else
	info "Build of $name succeeded."
fi
